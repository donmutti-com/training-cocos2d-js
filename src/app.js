var AppLayer = cc.Layer.extend({
    sprite: null,
    ctor: function () {
        this._super();
        var size = cc.winSize;

        var bg = new cc.Sprite();
        bg.setTexture(R.img.MAIN_MENU_BG);
        bg.x = size.width / 2;
        bg.y = size.height / 2;
        this.addChild(bg, 0);

        var gameTitle = new cc.LabelTTF(R.text.GAME_TITLE, "Candara", 42, null, cc.TEXT_ALIGNMENT_LEFT);
        gameTitle.x = 20 + gameTitle.width / 2;
        gameTitle.y = size.height - gameTitle.height / 2 - 20;
        this.addChild(gameTitle, 1);

        var menuStart = new cc.MenuItemFont("Start", function () {
            cc.director.runScene(new cc.TransitionFade(1, new GameScene()));
        }, this);
        var menu = new cc.Menu(menuStart);
        this.addChild(menu);

        return true;
    }
});

var AppScene = cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer = new AppLayer();
        this.addChild(layer);
        return true;
    }
});

