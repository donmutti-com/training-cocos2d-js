var GameLayer = cc.Layer.extend({

    star: null,
    center: cc.Point,
    radius: 60,
    angle: 0,

    ctor: function () {
        this._super();
        this.star = null;
        return true;
    },

    onEnter: function () {
        this._super();

        var size = cc.winSize;

        // Create background
        var bg = new cc.Sprite();
        bg.setTexture(R.img.GAME_BG);
        bg.x = size.width / 2;
        bg.y = size.height / 2;
        this.addChild(bg, 0);

        // Create star
        this.center = cc.p(size.width / 2, size.height / 2);
        this.star = new cc.Sprite(R.img.STAR);
        this.addChild(this.star);

        // Schedule game tick
        this.schedule(this.doTick, Constants.eachTick);

        // Register keys
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed: this.onKeyPressed,
            onKeyReleased: this.onKeyReleased,
        }, this);

        return true;
    },

    doTick: function () {
        var newX = this.center.x + Math.cos(this.angle) * this.radius;
        var newY = this.center.y + Math.sin(this.angle) * this.radius;
        this.angle += 0.05;

        this.star.setPosition(newX, newY);
    },

    doOver: function() {
        cc.director.runScene(new cc.TransitionFade(1, new AppScene()));
    },

    onKeyPressed: function (key, event) {
        if (key === 27) {
            event.getCurrentTarget().doOver();
            //this.doOver();
        }
    },

    onKeyReleased: function (key, event) {
    }


});


var GameScene = cc.Scene.extend({
    onEnter: function () {
        this._super();
        this.addChild(new GameLayer());
        return true;
    }
});