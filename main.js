cc.game.onStart = function () {

    // Pass true to enable retina display, on Android disabled by default to improve performance
    cc.view.enableRetina(cc.sys.os === cc.sys.OS_IOS);

    // Adjust viewport meta
    cc.view.adjustViewPort(true);

    // Setup the resolution policy and design resolution size
    //cc.view.setDesignResolutionSize(960, 500, cc.ResolutionPolicy.SHOW_ALL);

    // Instead of set design resolution, you can also set the real pixel resolution size
    cc.view.setRealPixelResolution(960, 500, cc.ResolutionPolicy.SHOW_ALL);

    // The game will be resized when browser size change
    cc.view.resizeWithBrowserSize(true);

    // Load resources
    cc.LoaderScene.preload(g_resources, null, this);

    // Run main menu scene
    cc.director.runScene(new AppScene());
};

cc.game.run();